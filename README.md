# AutoDoor

Opens/closes door based on a command from 24V signal. The process of closin/opening is esatimated to take 3.5s. Upon starting the system it should first check Its position and calibrate/move to origin in a slow manner, which is in this case **door open**. After it repeatedly checks signal state and door state until the is a change. **safety** when the operation takes longer than expected it should stop and waint until told diffrently.

   Automatic door system on CNC Doosan DNM 4500:
    -24V activation signal,
    -reley Ui = 5V,
    -24V PSU,
    -12V PSU,
    -Manual switch (CO)
    -2 endstops,
    -NEMA 34 w/ DM860I.

   PINOUTS:
    -*as declared*
    - MAN_CLOSE & MAN_OPEN not used.

    Warnings:
     -do not add serial comm while running motor without for loop

    TODO switching from manual to automatic bugs out
    TODO calculate threshold time

Made by Luka Knapič
