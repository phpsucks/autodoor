/*
   Made by Luka Knapič

   Automatic door system on CNC Doosan DNM 4500:
    -24V activation signal,
    -2 endstops,
    -NEMA 34 w/ DM860I.

   PINOUTS:
    -*as declared*
    - MAN_CLOSE & MAN_OPEN not used.

    Warnings:
     -do not add serial comm while running motor without for loop

    TODO switching from manual to automatic bugs out
    TODO calculate threshold time
    

*/

////////////////INPUTS//////////////////////////

#define SENSOR_CLOSED 10
#define SENSOR_OPEN 2
#define START_SIGNAL 3
#define MANAUTO 9
#define MAN_CLOSE 8
#define MAN_OPEN 7


////////////////OUTPUTS//////////////////////////

#define DIR 4
#define PULSE 5
#define ENABLE 6

////////////////VARABLES/CONSTANTS//////////////////////////
#define threshold 6000 // time threshold(safety)
bool man_auto = 0;
bool state = NULL;

////////////////SETUP//////////////////////////
void setup() {
  pinMode(SENSOR_CLOSED, INPUT_PULLUP);
  pinMode(SENSOR_OPEN, INPUT_PULLUP);
  pinMode(START_SIGNAL, INPUT_PULLUP);
  pinMode(MAN_CLOSE, INPUT);
  pinMode(MAN_OPEN, INPUT);
  pinMode(MANAUTO, INPUT);

  pinMode(ENABLE, OUTPUT);
  pinMode(DIR, OUTPUT);
  pinMode(PULSE, OUTPUT);
 
  bootup();
}

////////////////MAGIC//////////////////////////

void runMotor(int pulse, int dir){
    digitalWrite(DIR, dir);
    digitalWrite(PULSE, 1);
    delayMicroseconds(pulse);
    digitalWrite(PULSE, 0);
    delayMicroseconds(pulse);
  
}

void bootup() {
  if (doorCheck() == NULL || doorCheck() == 0) { // bootup to open slow ~5s
    slowOpen();
  }
}

void manOrAuto() {  // check switch state

  if (digitalRead(MANAUTO)) {
    man_auto = 1;
  }
  else if (!digitalRead(MANAUTO)) {
    man_auto = 0;
  }

}

bool doorCheck() {  //door state open/close return null if not at endpoint
  

  if (!digitalRead(SENSOR_OPEN) && digitalRead(SENSOR_CLOSED)) // TRUE door open
    state = 1;

  else if (digitalRead(SENSOR_OPEN) && !digitalRead(SENSOR_CLOSED)) // FALSE door closed
    state = 0;
  //Serial.println(digitalRead(SENSOR_OPEN));
  return state;
}

bool signalCheck() {  // check system command
  if (!digitalRead(START_SIGNAL)) // TRUE when open is needed
    return 1;
  else if (digitalRead(START_SIGNAL)) // FALSE when close is needed
    return 0;
  else
    exit(1);
}

void doorClose() {


  int start_time = millis();
  int time_passed = 0;

  while (doorCheck() != 0 && time_passed < threshold) {
    time_passed = millis() - start_time;
   for(int i = 0; i < 20; i++){
      runMotor(1, 1);
    }

  }
}

void doorOpen() {
  digitalWrite(DIR, 0);

  int start_time = millis();
  int time_passed = 0;

 while (doorCheck() != 1 && time_passed < threshold) {
    time_passed = millis() - start_time;
    //Serial.println("door is opening");
    for(int i = 0; i < 20; i++){
      runMotor(1, 0);
    }
  }
}

void slowOpen() {



  int start_time = millis();
  int time_passed = 0;

  while (!doorCheck() && time_passed < threshold + 3000) {
    time_passed = millis() - start_time;
    //Serial.println("here");
    
      runMotor(20, 0);
    
  }
}

void autoRun() {
  bool door_state = doorCheck();
  bool signal_state = signalCheck();
  //Serial.println(signal_state);
  //Serial.println(door_state);
  if (door_state == signal_state) {
    return;
  }

  else if (door_state != signal_state) {

    if (signal_state && !door_state) {
      doorOpen();
    }
    else if (!signal_state && door_state) {
      doorClose();
    }
  }


}

////////////////LOOP//////////////////////////

void loop() {

  manOrAuto();

  if (man_auto) {

    //digitalWrite(ENABLE, 1);
    autoRun();
  }
  else if (!man_auto) {
    //digitalWrite(ENABLE, 0);
  }


}
